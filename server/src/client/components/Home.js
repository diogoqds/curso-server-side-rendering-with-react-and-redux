import React from 'react';

const Home = () => {
  return (
    <div>
      <div>Home component</div>
      <button onClick={() => console.log('Hi')}>Press</button>
    </div>
  );
}

export default Home;